<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sent_mails', function (Blueprint $table) {
            $table->increments('sentmail_id');
            $table->integer('group_id')->unsigned();
            $table->string('sentmail_subject',120);
            // $table->string('sentmail_group',60);
            $table->string('sentmail_attachment', 150)->nullable();
            $table->text('sentmail_message');
            $table->dateTime('sentmail_on');
            $table->timestamps();

            $table->foreign('group_id')
                 ->references('id')->on('groups')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sent_mails');
    }
}
