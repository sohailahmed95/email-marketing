<!doctype html>
<html lang="en">
<head>

  @include('inc.header')
  <!-- Required meta tags -->

  <meta charset="utf-8">
  <title>Add User</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}

  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <style>
    #userform .error {
      display: block;
      width: 100%;
      margin-bottom: 0;
      color: red;
      line-height: normal;
    }
    select#group_id { color: #495057 !important; }
  </style>
</head>
<body>
  <div class="modal-dialog">
    <!-- Modal content-->
        @if(count($errors)>0)
        <div class="alert alert-danger">
          Errors <br>
          <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
    <div class="modal-content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
          </div>
    
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
            
        <h4>Create New Group</h4>
          </div>
          <div class="col-md-4">

         <button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#groupName">Remove</button>
        </div>
        </div>
        <div class="row pt-3">
        <div class="col-md-12">
          
        
        <form id="groupForm" action="{{ route('create-group')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" class="form-control input" placeholder="Name" id="updateid">
          
          
          
          <div class="form-group row">
            <div class="col-md-12">
              <input  type="text" required name="group_name" class="form-control input" placeholder="Group Name" id="group_name">
            
            </div>
     
          </div>
          <div>
        
          </div>
          <div class="btnsSave text-center"><br>
            <button type="submit" class="btn btn-success">Save</button><br>
          </div><br>
        </div>
      </div>
    </form>
  </div>
</div> 
 </div>
</div>
<!-- Modal -->
<div class="modal fade" id="groupName" tabindex="-1"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remove Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="errors">
          <ul class="alert-danger"></ul>
        </div>
        <form id="ajax">
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="icmn-key"></i>
              </span>
               <select name="group_id" class="form-control" id="group_id" placeholder="select Group" >

                @foreach ($groups as $group)
                <option  value=""selected hidden>Select Group</option>
                <option  value="{{$group->id}}">{{ $group->group_name }}</option>
                @endforeach
              </select>
              {{-- <input  type="text" required name="group_name" class="form-control input" placeholder="Group Name" id="group_name"> --}}
            </div>
            <span class="text-danger" id="error_password"> </span>
          </div>
          <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
      </div>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
        <button type="button" class="btn btn-danger" id="delete_group">Remove</button>
      </div>

    </div>
  </div>
</div>

<!-- Script -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>

<<script>

    $(document).ready(function(){

    // $(document).on('click','#add_data',function(){
    //   alert('sdfjkghjkdfgh')
    // });

    $('#delete_group').click(function(){
      $.ajax({
        type: "post",
        url: "{{ route('delete-group')}}",
        data: $('#ajax').serialize(),

        success: function(data){
          alert(data.message);
          location.reload();


        },

        error: function(data){
          alert('error');
        }
      });
    });
  });

  $('#groupForm').validate({
    rules: {   
      group_name: {
        required: true,
      }}
    });
  </script>
</body>
</html>