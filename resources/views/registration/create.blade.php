<!doctype html>
<html lang="en">
<head>

  @include('inc.header')
  <!-- Required meta tags -->

  <meta charset="utf-8">
  <title>Add User</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}

  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <style>
    #userform .error {
      display: block;
      width: 100%;
      margin-bottom: 0;
      color: red;
      line-height: normal;
    }
    select#group_id { color: #495057 !important; }
  </style>
</head>
<body>
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" style="margin-left: 167px"     >Register User</h4>
      </div>
      <div class="modal-body">
        <form id="userform" action="{{ action('RegistrationController@storeDevice') }}" method="post">
          {{ csrf_field() }}
          <input type="hidden" class="form-control input" placeholder="Name" id="updateid">
          <div class="form-group row">

            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="icmn-user"></i>
                </span>
                <input type="text" name="first_name" class="form-control input" placeholder="First Name"  id="name" >


              </div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="icmn-user"></i>
                </span>
                <input type="text" name="last_name" class="form-control input" placeholder="Last Name" id="last_name" >


              </div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="icmn-mail2"></i>
                </span>
                <input id="email" type="email" class="form-control" placeholder="Email" name="email" >

              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <select name="group_id" class="form-control" id="group_id" placeholder="select Group" >

                @foreach ($groups as $group)
                <option  value=""selected hidden>Select Group</option>
                <option  value="{{$group->id}}">{{ $group->group_name }}</option>
                @endforeach
              </select>
            </div>
           {{--  <div class="col-md-3">
              <button type="button" class="btn btn-primary w-100" data-toggle="modal" data-target="#groupName">+ Add</button>
            </div> --}}
          </div>
          <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
          </div>
          <div class="btnsSave text-center"><br>
            <button type="submit" class="btn btn-success">Save</button><br>
          </div><br>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="groupName" tabindex="-1"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="errors">
          <ul class="alert-danger"></ul>
        </div>
        <form id="ajax">
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="icmn-key"></i>
              </span>
              <input  type="text" required name="group_name" class="form-control input" placeholder="Group Name" id="group_name">
            </div>
            <span class="text-danger" id="error_password"> </span>
          </div>
          <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_group">Add</button>
      </div>

    </div>
  </div>
</div>

<!-- Script -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>

<script type='text/javascript'>
  $(document).ready(function(){

    // $(document).on('click','#add_data',function(){
    //   alert('sdfjkghjkdfgh')
    // });

    $('#save_group').click(function(){
      $.ajax({
        type: "post",
        url: "{{ url('createGroup') }}",
        dataType: "json",
        data: $('#ajax').serialize(),

        success: function(data){

          if(data.status){

            alert('New User Added!')
            $("#group_id").html('');
            var option = '';
            var data = data.data;
            Object.keys(data).forEach(key => {
              option += "<option value='"+data[key].id+"'>"+data[key].group_name+"</option>";

            });
            $("#group_id").html(option);
            $('#groupName').modal('hide');
          }

          else{
            var li ='';
            Object.entries(data.error).forEach(entry => {
              let key = entry[0];
              let value = entry[1];
              li += "<li>"+value+"</li>";
              $(".errors ul").append(li);
            });
          }
        },

        error: function(data){
          alert('error');
        }
      });
    });
  });


  $('#userform').validate({
    rules: {
      first_name: {
        minlength: 2,
        required: true
      },
      last_name: {
        required: true,
        minlength: 2,
      },
      email: {
        required: true,
      },
      group_id: {
        required: true,
      }}
    });
  </script>
</body>
</html>