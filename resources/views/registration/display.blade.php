<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Users Record</title>
    
    @include('inc.header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">User Data</div>
            <div class="flash-message">
              @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))
              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              @endif
              @endforeach
            </div>
            <div class="panel-body">
              <table class="table" id="myTable">
                
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Email</td>
                    <td>Group</td>
                    <td>Action</td>
                  </tr>
                </thead>
                
                <tbody>
                  @foreach($tests as $test)
                  <tr>
                    <td>{{$loop->iteration }}</td>
                    <td>{{$test->first_name}}</td>
                    <td>{{$test->last_name}}</td>
                    <td>{{$test->email}}</td>
                    <td>{{isset($test->group) ? $test->group->group_name : 'N/A'}}</td>
                    <td>
                      <form action="{{ route('test.destroy',$test->id) }}" method="POST">
                        
                        
                        <a class="btn btn-primary" href="{{ route('test.edit',$test->id) }}">Edit</a>
                        {{ csrf_field() }}
                        
                        {{ method_field('DELETE') }}
                        <button  onclick="return confirm('Are you sure?')" class="btn btn-danger" type="submit">Delete</button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
    
    <script>
    $(document).ready( function () {
    // $('#myTable').DataTable();
    $('#myTable').DataTable( {
    "order": [[ 0, "desc" ]]
    } );
    $('.close').on('click', function(){
    $('.flash-message').hide();
    })
    } );
    //     $('.close').on('click', function(){
    //       $('.flash-message').hide();
    //     })
    // } );
    </script>
    
  </body>
</html>