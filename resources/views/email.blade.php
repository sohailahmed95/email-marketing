<!DOCTYPE html>
<html lang="en">
    <head>
        @include('inc.header')
        <meta charset="UTF-8">
        <title>New Email</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                    </div>
                    <form action="{{route('mail')}}" method="post" enctype="multipart/form-data" >
                        <div class="emailFormInner" >
                            
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" name="subject"  value="{{ old('subject')}}" id="subject" placeholder="Type Subject">
                                    @if($errors)
                                    <span class="text-danger">
                                        {{ $errors->first('subject')}}
                                    </span>
                                    @endif
                                </div>
                                
                                <div class="col-sm-6">
                                    <label for="select-group">Select Group</label>
                                    
                                    <select name="select_group" class="form-control" id="select_group" placeholder="Select Group">
                                        <option value="" selected hidden>Select Group</option>
                                        @foreach ($groups as $group)
                                        @if($group->test==null)
                                        @else
                                        <option  value=" {{ $group->id }}" {{ old('select_group') == $group->id  ? 'selected' : '' }} >{{ $group->group_name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @if($errors)
                                    <span class="text-danger">
                                        {{ $errors->first('select_group')}}
                                    </span>
                                    @endif
                                    <span class="text-danger">
                                        
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label for="select_file" >Select Attachment</label>
                                    <input  multiple="multiple" type="file" class="form-control" name="select_file[]" id="select_file" onchange="javascript:updateList()"></input>
                                    <p id="file-name"></p>
                                    {{-- <input type="file" name="select_file[]" id="select_file" multiple> --}}
                                </div>
                                <div class="form-group">
                                    <label for="editor">Message</label>
                                    <textarea name="editor" id="editor" class="form-control"  placeholder="Select Group" >{{ old('editor')}}</textarea>
                                    @if($errors)
                                    <span class="text-danger">
                                        {{ $errors->first('editor')}}
                                    </span>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        
                        <input class="btn btn-sm btn-success" type="submit"></input>
                    </div>
                    </form>
                    {{--  <form id="logout-form" action="{{ route('mail') }}" method="POST" style="display: none;">
                    </form> --}}
                </div>
            </div>
            {{-- <script src="script.js"></script> --}}
            <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
            <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
            <script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
        </body>
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);
        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
        $('.close').on('click', function(){
        $('.flash-message').hide();
        })
        CKEDITOR.replace( 'editor', {
        toolbar: [
        [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo', 'Clipboard' ],
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        '/',
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
        { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] }
        ]
        });
        });

        </script>
    </html>