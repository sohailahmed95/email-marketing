<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Email</title>
		<style type="text/css">
		
		
		</style>
	</head>
	<body>
		
		<div class="editor">{!!$data['editor']!!}</div>
		@if(isset($data['file_name']))
		<h5>Attachments:</h5>
		@foreach($data['file_name'] as $file)
		<div>
			<a href="{{$message->embed(public_path() . '/uploads/'.$file) }}" target="_blank">
				<button class="btn btn-xs"><i class="fa fa-download"></i> {{$file}}</button>
			</a>
		</div>
		@endforeach
		@endif
	</body>
</html>