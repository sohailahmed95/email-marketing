<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Sent Mails Record</title>
    
    @include('inc.header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">User Data</div>
            <div class="flash-message">
              @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))
              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              @endif
              @endforeach
            </div>
            <div class="panel-body">
              <table class="table" id="myTable">
                {{-- @php(dd($tests)); --}}
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Subject</td>
                    <td>Group</td>
                    <td>View Message</td>
                    <td>Attachments</td>
                  </tr>
                </thead>
                
                <tbody>
                  @foreach($tests as $test)
                  <tr>
                    <td>{{$test->sentmail_id }}</td>
                    <td >{{$test->sentmail_subject}}</td>
                    <td>
                 
              <a  class="btn btn-secondary text-white w-100" id="email_groupid" onclick="sentmails_group_list({{$test->group->id}})" data-toggle="modal" data-target="#myModal1">{{$test->group->group_name}}</a>
            
                    </td>
                    <td >
                   <a  class="btn btn-success text-white w-100" id="message_groupid" onclick="sentmails_group_content({{$test->sentmail_id}})" data-toggle="modal" data-target="#myModal2">View Message</a>
                    </td>

                    <td> 
                      @if(json_decode($test->sentmail_attachment) != null)
                      @foreach(json_decode($test->sentmail_attachment) as $attachment) 
                    <li><a href="{{ route('download_file',$attachment)}}">{{ $attachment}}</a></li>    
                      @endforeach
                      @else
                      <p>No attachments</p>
                      @endif
                    </td>

                  </tr>
                  @endforeach
                </tbody>
              </table>              
            </div>
          </div>
        </div>
      </div>
    </div>
<div class="modal fade" id="myModal1" tabindex="-1"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Members</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="ajax">
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="icmn-key"></i>
              </span>
              <div id="e-list">
                <ul>
                  
                </ul>
              </div>
            </div>
            <span class="text-danger" id="error_password"> </span>
          </div>
          <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary" onclick="message" id="save_group">Add</button> --}}
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Email Content</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="ajax">
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="icmn-key"></i>
              </span>

                <div class="col-md-12">
                  <div>
                    
                    <h5><b>Subject:</b></h5>
                  </div>
                  <div class="mails-modal">
                    <p id="e-sub"></p>
                  </div>
                </div>
                <div class="col-md-12">
                  <div>
                    
                    <h5><b>Message:</b></h5>
                  </div>
                  <div class="mails-modal">
                    <p id="e-content"></p>
                  </div>
                </div>

            </div>
            <span class="text-danger" id="error_password"> </span>
          </div>
          <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary" onclick="message" id="save_group">Add</button> --}}
      </div>
    </div>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
  <script type="text/javascript" src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
 


  <script type="text/javascript">


function sentmails_group_list(id) {

    var url = "{{ route('showMembers') }}" + "/" + id;
    $.ajax({
        type: "get",
        dataType: 'json',
        url: url,
        success: function(data) {
            var li = '';
            var data = data.data;

            Object.entries(data).forEach(entry => {
                let key = entry[0];
                let value = entry[1];
                li += "<li><svg width='10px' height='10px' aria-hidden='true' focusable='false' data-prefix='fas' data-icon='chevron-right' class='svg-inline--fa fa-chevron-right fa-w-10' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512'><path fill='currentColor' d='M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z'></path></svg>" + data[key].email + "</li>";
                $("#e-list ul").html(li);
            });
        },
        error: function(data) {
            console.log('Something is wrong');
        }
    });
}

function sentmails_group_content(id) {

    var url = "{{ route('showContent') }}" + "/" + id;
    $.ajax({
        type: "get",
        dataType: 'json',
        url: url,
        success: function(data) {
            var content = '';
            var subject = '';
            var data = data.data;
            // var keysss = Object.keys(data)
            // console.log(data.sentmail_message);

            // Object.entries(data).forEach(entry => {
                // let key = entry[0];
                // let value = entry[1];
                // content = "<p>" + data.sentmail_message + "</p>";
                // subject = "<p>" + data.sentmail_subject + "</p>";
                $("#e-content").html(data.sentmail_message);
                $("#e-sub").html(data.sentmail_subject);
            // });
        },
        error: function(data) {
            console.log('Something is wrong');
        }
    });
}



  $('#myTable').DataTable( {
  "order": [[ 0, "desc" ]]
  } );

  </script>
  
</body>
</html>