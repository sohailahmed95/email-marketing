<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  
  <title></title>

</head>
<body>


  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{ url('/email') }}">Email Template</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item  {{\Request::is('email') ? 'active' : ''}}"> <a class="nav-link" href="{{ url('/email') }}">Email <span class="sr-only">(current)</span></a></li>
      <li class="nav-item  {{\Request::is('createMember') ? 'active' : ''}}">
        <a class="nav-link" href="{{ url('/createMember') }}">Add User</a>
      </li> 
      <li class="nav-item  {{\Request::is('createGroup') ? 'active' : ''}}">
        <a class="nav-link" href="{{ url('/createGroup') }}">Groups</a>
      </li>
      <li class="nav-item  {{\Request::is('import-users') ? 'active' : ''}}">
        <a class="nav-link" href="{{ route('import_users') }}">Import Users</a>
      </li>
   
      <li class="nav-item  {{\Request::is('display') ? 'active' : ''}}"> <a class="nav-link" href="{{ url('/display') }}">User List</a></li>
      <li class="nav-item  {{\Request::is('emails') ? 'active' : ''}}"> <a class="nav-link" href="{{ url('/emails') }}">Sent Emails</a></li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <!-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->


       <!-- <a href="{{ url('/logout') }}" classs="btn btn-primary " type="button"> logout </a> -->
       <a class="btn btn-primary" href="{{ url('/logout') }}" classs="btn btn-danger " role="button">logout </a> 

    </form>

    {{-- <<link rel="alternate" href="{{ asset('public/js/app.js')}}"> --}}


  </div>
</nav>
  
</body>
</html>