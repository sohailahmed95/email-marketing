<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Jobs\SendEmailJob;

// Route::get('emailtest', function(){
  
// 	$details['email'] = 'your_email@gmail.com';
  
//     dispatch(new App\Jobs\SendEmailJob('your_email@gmail.com'));
  
//     dd('done');
// });


Route::get('/', function () {
    return view('auth.login');
});

 
Route::get('/createMember', 'RegistrationController@create');
Route::post('createMember', 'RegistrationController@store');

Route::post('/createMember','RegistrationController@storeDevice');
Route::get('/import-users','RegistrationController@importUsers')->name('import_users');
Route::post('/storeExport','RegistrationController@storeExport')->name('storeExport');

Route::resource('test', 'RegistrationController');

Route::get('/createGroup','RegistrationController@createGroup');


Route::get('view_members/{id?}','SentMailsController@membersList')->name('showMembers');
Route::get('view_content/{id?}','SentMailsController@emailContent')->name('showContent');


Route::post('/saveGroup', 'RegistrationController@saveGroup')->name('create-group');
Route::post('/deleteGroup', 'RegistrationController@deleteGroup')->name('delete-group');
Route::get('getGroup/{id}', 'RegistrationController@getGroup');

Route::get('/logout', 'RegistrationController@logout');

Route::get('/email', 'EmailController@create')->name('mail.index');

Route::get('/display', 'RegistrationController@display')->name('display');


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home','HomeController@index')->name('home');


Route::get('/emails', 'SentMailsController@index');

Route::post('/mail', 'EmailController@mail')->name('mail');
Route::get('/download/{path?}', 'SentMailsController@download_file')->name('download_file');


// Route::get('/', 'Home2Controller@index');
// Route::get('/send', 'Home2Controller@send');



// Route::get('/test123', function() {
//         $emailJob = new SendEmailJob(Auth::user());
//         dispatch($emailJob);
// });



// Route::get('emailtest', 'EmailController@mail');


// Route::get('email-test', function(){
//    Artisan::call('mails:scheduledmails');

//  // $details['email'] = 'your_email@gmail.com';
  
//  //    if(dispatch(new App\Jobs\SendEmailJob($details))){
//  //    	dd('ok');
//  //    }
//  //    else{
//  //    	dd('not ok');
//  //    }
  
// });