<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Test;
use App\Group;
use App\SentMails;
use App\Mail\SendMailable;
use App\Jobs\SendQueuedMails;

class EmailController extends Controller
{
public function __construct()
  {
      $this->middleware('auth');
  }
public function create(Test $test, Group $group)
  {
      $tests = Test::all();
      $groups = Group::all();
      return view('email', compact('test','groups'));
  }
  

public function mail(Request $request)
       {
       $file_loc = "";
       $current_date_time = Carbon::now();
       $rules = [
           'subject' => 'required',
           'select_group' => 'required',
           'editor' => 'required'
       ];
       $errormsg = [
          'required' => 'This field is required.'
       ];
       $validator = $this->validate($request, $rules, $errormsg);
       $groupid = $request->select_group;
       $users = Test::select('*')->where('group_id',$groupid)->whereNotNull('group_id')->get();
       $data['subject'] = $request->subject;
       $data['group_name'] = $request->select_group;
       $data['editor'] = $request->editor;

       if( $validator){

       $files =  $request->file('select_file');
       if (isset($files)) { 

        foreach ($files as $key=> $file) {
         
         $name = Storage::put('app/public', ($file->getClientOriginalName()));
         $file_loc = $file->move(public_path('/uploads'), $file->getClientOriginalName());
         // $path =  $file_loc->getRealPath();
              
         $data['file_name'][$key] = $file->getClientOriginalName();
          
       }
     }
      else{
       }

       foreach($users as $user)
        {
           Mail::to($user->email)->queue(new \App\Mail\SendMailable($data));

        }
       $member = new SentMails();
       $member->sentmail_subject = $request->input('subject');
       $member->group_id = $request->input('select_group');
       $member->sentmail_attachment = isset($data['file_name']) ? json_encode($data['file_name']) : json_encode([]);
       $member->sentmail_message = $request->input('editor');
       $member->sentmail_on = $current_date_time;
       $member->save();
       $request->session()->flash('alert-success', 'Email has been sent!');
       return redirect()->back();
     }
     // else
     // {
     //   return redirect()->back();
     // }
   // }
}
}