<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\SentMails;


class SentMailsController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
   }
   
    public function index()
    {
        $tests = SentMails::all();
          
         return view('mails',compact('tests'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }

        public function membersList($id){

        $members = Test::select('*')->where('group_id',$id)->get();
        return response()->json(['status'=>true,'data'=>$members]);

        }

        public function emailContent($id){
        $content = SentMails::where('sentmail_id',$id)->first();
        return response()->json(['status'=>true,'data'=>$content]);

        }

        public function download_file($attachment){
        $file_path = public_path('/uploads/'.$attachment);
        return response()->download($file_path);
        }
}
