<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Test;
use App\Group; 
use DB;
use Redirect;

class RegistrationController extends Controller
{

 public function __construct()
    {
        $this->middleware('auth');
    }


 public function index()
    {
        $test = Test::latest()->paginate(5);
  
        return view('registration.display',compact('test'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

public function display(){



        $tests = Test::all();
        return view('registration.display',compact('tests'));
}

    public function create()
    {
        $groups = Group::all();
        return view('registration.create',compact('groups'));
    }
    
    public function storeDevice(Request $request){
        $group = new Test();

        $group->first_name = $request->input('first_name');
        $group->last_name = $request->input('last_name');
        $group->email = $request->input('email');
        $group->group_id = $request->input('group_id');
        $group->save();

        $request->session()->flash('alert-success', 'User was successful added!');
 
        return redirect()->back();
    }


    public function storeExport(Request $request){

        $rules = [
            'users_file' => 'required|mimetypes:application/octet-stream,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];
        $errors = [
            'users_file.required' => 'Please select a file',
            'users_file.mimetypes' => 'Select only XLS/XLSX file',
        ];
        $validator = Validator::make($request->all(), $rules, $errors);
        if ($validator->fails()) {
          return redirect('import-users')
                        ->withErrors($validator)
                        ->withInput();
        }


        $path = $request->file('users_file')->getRealPath();
            $data  = Excel::load($path, function($reader) { 
            $reader->calculate(false);
        });
        
        foreach($data->toArray() as $value)
        {
          if(isset($value['first_name']) && isset($value['last_name']) && $value['email']){
             $insert[] = array(
                'first_name'  => $value['first_name']  ?? null ,
                'last_name'   => $value['last_name'] ?? null  ,
                'email'   => $value['email']  ?? null ,
                'group_id'   => $request->group_id  ?? null ,
            );
          }
          else{
            $insert = [];
          }
        }
        if(!empty($insert)){
            DB::table('tests')->insert($insert);
            $request->session()->flash('alert-success', 'Data Imported successfully');
        }
        else{

            $request->session()->flash('alert-danger', 'Required data not exists in file');
        }

        return redirect()->back();
    }


  public function createGroup(Request $request){
        $groups = Group::all();
        return view('registration.group', compact('groups'));
         
 }

  public function saveGroup(Request $request){
        
        $rules = [
            'group_name' => 'required|unique:groups'
        ];
       
        $errormsg = [
           'required' => 'Please enter group name'
        ];

        $validator = Validator::make($request->all(), $rules, $errormsg);

        if ($validator->fails()) {
            $request->session()->flash('alert-danger', $validator->errors());
        return redirect()->back();


        // return response()->json(['status'=>false,'error'=>$validator->errors()]);
        }
      
        $new_group=new Group;
        $new_group->group_name = $request->input('group_name');
        $new_group->save();    
        $groups = Group::all();
            $request->session()->flash('alert-success', 'New Group Created');

        return redirect()->back();
         
 }

  public function deleteGroup(Request $request){
    $group = Group::find($request->group_id);
    $group->delete();
    // $request->session()->flash('alert-danger', $validator->errors());
     return response()->json(['status'=>1,'message'=>'Group Deleted!']);
    // 

         
 }



 public function edit(Test $test, Group $group)
    {
        $tests = Test::all();
        $groups = Group::all();
        
        return view('registration.update', compact('test','groups'));
    }


public function update(Request $request,  $id){


{
        $test =  Test::find($id);

        $test->last_name = $request->input('last_name');
        $test->email = $request->input('email');
        $test->group_id = $request->input('group_id');
        $test->first_name = $request->input('first_name');

        $test->save();

         $request->session()->flash('alert-success', 'User has been  Updated successfully!');
             return redirect()->route('display');

    }

}

        public function destroy(Request $request, Test $test)
        {

        $test->delete();
        $request->session()->flash('alert-danger', 'User has been  Deleted successfully!');
        return redirect()->back();
        }


 public function logout(Request $request) {
  Auth::logout();
  return redirect('/login');
}

    
    public function importUsers()
    {
        $groups = Group::all();
        return view('registration.import',compact('groups'));
    }



}
