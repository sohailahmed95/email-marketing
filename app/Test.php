<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
	protected $table= 'tests';
	  protected $fillable = [
        'first_name', 'last_name', 'email','group_id'
    ];

    //

        /**
     * Get the phone record associated with the user.
     */
    public function group()
    {
    	// return $this->belongsTo('App\Model\Test\Group’, ‘group_id', 'id');
         return $this->belongsTo('App\Group');
    }
}
