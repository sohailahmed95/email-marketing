<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentMails extends Model
{
		protected $fillable = [
		'sentmail_subject', 'group_id', 'sentmail_attachment', 'sentmail_message','sentmail_on'
		];
 
	   public function group()
	    {
	    	// return $this->belongsTo('App\Model\Test\Group’, ‘group_id', 'id');
	         return $this->belongsTo('App\Group');
	    }

}
