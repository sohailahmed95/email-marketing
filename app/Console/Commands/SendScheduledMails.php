<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\SendEmailTest;
use Mail;

class SendScheduledMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails:scheduledmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send queued mails to the list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
   
        // Mail::to('asas@sdfd.com')->send(new SendEmailTest());
    }
}
