<?php
   require_once('includes/vars.php');
    $pageClass = "reference-an-conversation-form"; $pagename = "Harvard Conversation Reference Generator";$refer_page_checker = true;
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Harvard Conversation Reference Generator -  British Assignment Writers</title>
 
      <?php  include'includes/canonical.php'; ?>
      <?php include 'includes/css.php'; ?>
   </head>
   <body>
      <?php include 'includes/headersection-innerpages.php'; ?>
      <div class="inner-page aboutPage">
      <?php include 'includes/breadcrumb-innerPage.php'; ?>
      <div class="InnerPageContetn">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="">
              <div class="InnerPageContenttxt">
                <div class="HarwardSection harvard-radio-programme-reference-tool referencingBlog">
                  <div class="note">Using this simple referencing tool you can create a Harvard Conversation Reference in a couple of clicks.
                    <div class="notebtn">
                      <a href="<?= $base_url ?>harvard-reference-generator.php" class="templateBtn2 light-brown">Select a different source type</a>
                    </div>
                  </div>
                  
                  <form class="hardvadListingText harvard-reference-generator-Feilds referenceBlog" id="reference_conversation" method="POST" action="<?= $base_url ?>reference-an-conversation-generate.php">
                    <h1 class="lg-head InnerPageheadingOne">* Required Fields
                      <span class="InnerPageheadingBlue"></span>
                    </h1>
                    <div class="harvard-reference-generator-Forms">
                      <div class="col-md-11">
                        <div class="row">

                          <div class="row">
                            <div class="col-md-6">
                              <div class="hrfeildBox">
                                <h3>1. Author</h3>
                                <p>Enter the Surname and Initials of the speaker</p>
                                <div class="form-group">
                                  <label for="">Author Surname <i>*</i></label>
                                  <input type="text" class="form-control" placeholder="e.g Tolkien" name="author_surname">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                            <div class="hrfeildBox hrfeildBoxSecond">
                                <h3></h3>
                                <p></p>
                                <div class="form-group">
                                  <LABEL FOR="">Author Initial(s)</LABEL>
                                  <input type="text" class="form-control" placeholder="Type here" name="author_initial">
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="hrfeildBox">
                            <h3>2. Receiver *</h3>
                            <p>Enter the name of the person the conversation took place with</p>
                            <div class="form-group">
                              <input type="text" class="form-control" placeholder="e.g Harry Smith" name="receiver">
                            </div>
                          </div> 
                          <div class="hrfeildBox">
                            <h3>3. Year of conversation *</h3>
                            <p>Enter the year the conversation took place in YYYY format.</p>
                            <div class="form-group">
                              <input type="text" class="form-control" placeholder="e.g 1954" name="conversation_year">
                            </div>
                          </div>
                          <div class="hrfeildBox">
                            <h3>4. Date of conversation *</h3>
                            <p>Enter the date the conversation took place</p>
                            <div class="form-group">
                              <input type="text" class="form-control" placeholder="e.g 26 January 2018" name="conversation_date">
                            </div>
                          </div>
 
                          <div class="hrfeildBox">
                            <h3>CREATE REFERENCE</h3>
                            <p>Click the button below to generate your Harvard Conversation Reference. Don't worry, you can come back and edit the reference if you need to correct or add any information</p>
                            </div>
                            <div class="generateRadioBtn">
                              <button type="submit" class="templateBtn2 dark-brown">generate conversation reference</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
                  </div>
               </div>
            </div>
         </div>
 
      <!-- inner-page -->
      <?php include 'includes/footerSection.php'; ?>
  <script type="text/javascript">
  $(function(){

    $('#reference_conversation').validate({
        rules: {
            author_surname: {
                required: true
            },           
            receiver: {
                required: true
            },            
            conversation_year: {
                required: true
            }, 
            conversation_date: {
              required: true
            },
        },

        submitHandler: function(form) {
  form.submit();
        },
    });

  });
</script>

   </body>
</html>