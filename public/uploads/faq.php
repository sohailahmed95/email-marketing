<?php
require_once('includes/vars.php');
$pageClass = "privacy";
?>
<?php
$title = "British Dissertation Writers - Frequently Asked Questions";
$description = "British Dissertation Writer’s FAQ – Here are some answers to the most popular questions. Do not hesitate anymore contact us anytime if you have any other questions.";
$linki="https://www.britishdissertationwriters.co.uk/privacy-policy.php";
$keywords="Best Dissertation Writing Service, Dissertation Help, Dissertation Writing Service, UK Dissertations, Write My Dissertation, Professional Dissertation Writers, Custom Dissertations";
?>
<?php include 'includes/header.php'; ?>
</head>
<body>
 <?php include 'includes/top-area.php'; ?>
 <?php include 'includes/live-chat-right.php'; ?>
 <!-- ===== banner area===== -->
 <section class="banner-area top-area-inner">
  <div class="container">
   <div class="logo-section">
     <div class="row">
      <div class="col-md-4">
       <a href="<?php echo $base_url; ?>" class="mainlogo">
        <img src="images/main-logo.png" alt="";>
      </a>
    </div>
    <div class="col-md-8">
     <?php include 'includes/navigation.php'; ?> 
   </div>
 </div>
</div>
</div>
</section>
<!-- ===== header area===== -->
<!-- ===== banner inner area===== -->
<section class="banner-inner" style="background-image:url(images/banner-inner01.jpg);">
  <div class="container">
    <div class="banner-inner">Frequently Asked Questions</div>
  </div>
</section>
<!-- breadcrumb -->
<div class="brd-crmb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo $base_url; ?>">Home</a></li>
          <li class="active">Frequently Asked Questions</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- ===== quote-section for ipdad and mobile area===== -->
<?php //include 'includes/right-side-form.php'; ?>
<!-- ===== quote-section for ipdad and mobile area===== -->
<?php //include 'includes/form-area-ipad-mobile.php'; ?>
<?php include 'includes/side-pop-up-form.php'; ?>

<!-- ===== main area===== -->
<section class="content-inner-main">
 <section class="content-inner">
  <div class="inner-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="inner-content">
            <h2 class="heading2">Frequently Asked Questions</h2>

         
            
            <div class="panel-group faq-panel" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading " role="tab" id="ht">
                  <h4 class="panel-title">
                    <a class="" data-toggle="collapse" data-parent="#accordion" href="#ct" aria-expanded="false" aria-controls="ct">
                    What subjects does the service offer?
                    </a>
                  </h4>
                </div>
                <div id="ct" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="ht" aria-expanded="false" style="">
                  <div class="panel-body">
                  We offer a wide-ranging stream of subjects as we possess specialist writers who can write regarding any pertaining topic, with utter ease and finesse. Therefore, students mustn’t be distressed when handed out a complex writing task, as our writers can deal with just about anything. 
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading green" role="tab" id="ho">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#co" aria-expanded="true" aria-controls="co" class="collapsed">
                    Are these services reasonably priced?
                    </a>
                  </h4>
                </div>
                <div id="co" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ho" aria-expanded="true" style="height: 0px;">
                  <div class="panel-body">
                  We at British Dissertation Writers have made it a point to present our customers with work which can be availed at an economical and reasonable price point.
                  </div>
                </div>
              </div>
            
            
              <div class="panel panel-default">
                <div class="panel-heading " role="tab" id="htr">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#ctr" aria-expanded="false" aria-controls="ctr">
                    Will my work be delivered right on time?
                    </a>
                  </h4>
                </div>
                <div id="ctr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="htr" aria-expanded="false">
                  <div class="panel-body">
                  Our customers mustn’t worry about the orders placed with us, as we see to it that each of our orders is delivered right at the mentioned time. 
                  </div>
                </div>
              </div>
              
              
              
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hf">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#cf" aria-expanded="false" aria-controls="cf">
                    Will my identity be protected?
                    </a>
                  </h4>
                </div>
                <div id="cf" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hf" aria-expanded="false">
                  <div class="panel-body">
                  We are equipped with a strong and firm Data Protection Policy, which provides broad-spectrum protection of personal information. Therefore, there shall be no incidents of any data leak.
                  </div>
                </div>
              </div>
              
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hfiv">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#cfiv" aria-expanded="false" aria-controls="cfiv">
                    How can I make my payment?
                    </a>
                  </h4>
                </div>
                <div id="cfiv" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hfiv" aria-expanded="false">
                  <div class="panel-body">
                  The payments for your orders can be made via PayPal or money can be deposited into our company’s bank account. The details of our bank account can be acquired from our customer care personnel.
                  </div>
                </div>
              </div>
              
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hsx">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#csx" aria-expanded="false" aria-controls="csx">
                    Can I contact my writer?
                    </a>
                  </h4>
                </div>
                <div id="csx" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hsx" aria-expanded="false">
                  <div class="panel-body">
                  Our customers can easily communicate with their writer by utilizing our ‘live chat’ option.
                  </div>
                </div>
              </div>
              
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hse">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#cse" aria-expanded="false" aria-controls="cse">
                    Are there any promotional discounts that I can utilise?
                    </a>
                  </h4>
                </div>
                <div id="cse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hse" aria-expanded="false">
                  <div class="panel-body">
                    We have discounts and offers that are available around the year, these can be availed by visiting our website or by either contacting our customer care representatives. Moreover, our discounts vary according to the length and volume of the work, thus the greater the amount of work placed with us the greater the promotional offers you can avail.
                  </div>
                </div>
              </div>
            
            
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hsf">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#csf" aria-expanded="false" aria-controls="csf">
                    Can I expect a refund?       
                     </a>
                  </h4>
                </div>
                <div id="csf" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hsf" aria-expanded="false">
                  <div class="panel-body">
                  Our customers are eligible for a refund if our writer digresses from the stipulated criterion leading the student to acquire an ‘F’ grade or if the content delivered by us was more than 30% plagiarised.</div>
                </div>
              </div>
            
            
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hsg">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#csg" aria-expanded="false" aria-controls="csg">
                    How will I receive my academic work?       
                     </a>
                  </h4>
                </div>
                <div id="csg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hsg" aria-expanded="false">
                  <div class="panel-body">
                  All completed orders are sent to the customer on the email address provided to our customer care representatives. These papers can then be downloaded off their email.</div>
                </div>
              </div>
            
            
            
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hsh">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#csh" aria-expanded="false" aria-controls="csh">
                      Who will be working on my paper?
                   </a>
                  </h4>
                </div>
                <div id="csh" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hsh" aria-expanded="false">
                  <div class="panel-body">
                  A qualified and experienced individual, who shall be equipped with a wide-ranging knowledge about your specialist subject field.</div>
                </div>
              </div>
            
            
             
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hshs">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#cshs" aria-expanded="false" aria-controls="cshs">
                        Why are we considered one of the best academic writing services?       
                    </a>
                  </h4>
                </div>
                <div id="cshs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hshs" aria-expanded="false">
                <div class="panel-body">
                    We are regarded as one of the best academic facilities due to our affordability, on-time delivery and capacity to deliver precisely what the student demands.</div>
                </div>
              </div>
              
              
              
            
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="hsi">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#csi" aria-expanded="false" aria-controls="csi">
                        What kind of services do we offer?        
                    </a>
                  </h4>
                </div>
                <div id="csi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hsi" aria-expanded="false">
                    <div class="panel-body">
                        We at British Dissertation Writers offer dissertation writing help to students.
                    </div>
                </div>
              </div>
            
            
            
            </div>
         </div>
      </div>
    </div>
  </section>
</section>
<?php include 'includes/footer.php'; ?>
