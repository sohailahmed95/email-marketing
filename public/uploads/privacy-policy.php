<?php
require_once('includes/vars.php');
$pageClass = "privacyPolicyPage";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>British Assignment Writers, Privacy Policy</title>
  <meta name="description" content="Read our privacy policy. Order us an assignment and get exclusive discount!! ">
  <meta name="keywords" content="">
  <?php  include'includes/canonical.php'; ?>
  <?php include 'includes/css.php'; ?>

</head>
  <body>

<?php include 'includes/headersection.php'; ?>


  <div class="inner-page privacyPolicyPage">

  <?php include 'includes/breadcrumb-innerPage.php'; ?>

    <div class="container">
        <div class="row">
          
          <div class="col-sm-12">
            <!-- <h1 class="heading1 page-title">Privacy Policy</h1> -->
            <br>
            <p>This policy covers how British Assignment Writers (“We”) treat and utilise personal information that we collect and receive from our visitors and customers. Our privacy policy is subject to timely change and the latest version will be posted on our website.</p>

            <p> <strong>Information Gathering:</strong> Personal information requested from you includes your name; email ID etc. which is used for the individual’s identification and communication purposes only.</p>

            <p><strong>Confidentiality Policy:</strong> We ensure confidentiality of the information we have stored. We deal with your personal information very carefully and will not pass it to non-concern third parties, as we are strong adherents of the Data Protection paired with the Companies Act Policy. However, we may only disclose the client’s information to legal authorities if necessitated by law.</p>

            <p><strong>Policy of Visit Information:</strong> We track your IP address in order to provide customised service. The information is extracted merely to enhance the knowledge base in order to offer customised products and services as per your region.</p>

            <p> <strong>Accession to Your Information:</strong> Whenever you access our website to acquire our rendered services, we aim to provide you access to your personal information by providing options to update any information you deem to change. This enables us to have complete and updated record with us in order to offer our services, along with have complete information for legal business processing. Additionally, in order to access your information, we verify your personal identity by asking you certain secret questions in order to ensure that information is going in the right hands.</p>

            <p> <strong>Details About Our Products & Services:</strong> Information pertaining to our products and services can be viewed on our website. Moreover, our promotional offers and discounts will be communicated to our customers through an email or a text on their phone number. </p>

            <p>For further Information, please contact <span class="text-pink phoneNumber"></span>.</p>
         
          </div>

        </div> <!-- row -->
      </div> <!-- container -->
  </div> <!-- inner-page -->


<?php include 'includes/footerSection.php'; ?>

</body>
</html>