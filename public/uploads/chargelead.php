<?php
require('includes/config.php');
if (isset($_SESSION['lead']['cart_order_id'])) {
  $order_id = $_SESSION['lead']['cart_order_id'];
}
else {
  die('Something went wrong!');
}
$data = getLeadDetailsPay($order_id);
$total_amount = ($_SESSION['lead'][ 'total']*100);
$error = FALSE;
$msg = '';
try {
  /**
   * [$token description]
   * @var [type]
   */
  $token  = $_POST['stripeToken'];
  $email  = $_POST['stripeEmail'];
  $customer = \Stripe\Customer::create(array(
    'email'  => $email,
    'source' => $token
  ));

  /**
   * [$charge description]
   * @var [type]
   */
  $charge = \Stripe\Charge::create(array(
    'customer' => $customer->id,
    'amount'   => $total_amount,
    'currency' => 'gbp',
    'description' => $order_id
  ));

  $str  = str_replace('Stripe\Charge JSON:','', $charge);
  $cus  = (json_decode($str, TRUE));

  /**
   * Update status to paid
   */
  // update('tbl_payments', ['status' => 'Paid','payment_method' => 'stripe' ,'updated_at' => $time_stamp, 'source' => $_POST['stripeToken'], 'stripe' => $charge, 'reference_stripe' => $cus['id']], 'order_id', $order_id);
  $insertData = [
    'lead_no'          => $order_id,
    'status'           => 'Paid',
    'amount'           => $_SESSION['lead'][ 'total'],
    'payment_method'   => 'stripe',
    'source'           => $_POST['stripeToken'],
    'stripe'           => $charge,
    'reference_stripe' => $cus['id'],
    'created_at'       => $time_stamp,
    'updated_at'       => $time_stamp
  ];
  // echo "<pre>".$charge; print_r($insertData); die;
  insert('tbl_lead_payments', $insertData);
  
  /**
   * [$insertData description]
   * @var [type]
   */
  $insertData = [
    'order_id'   => $order_id,
    'paid_date'  => date('Y-m-d'),
    'amount'     => $_SESSION['lead'][ 'total'],
    'statement'  => 'Full Payment',
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp
  ];
  insert('tbl_paid_amount', $insertData);
  
  /**
   * logs
   */
  insert('tbl_logs', ['order_id' => $order_id, 'action' => 'Payment Mode','detials' => 'stripe', 'user_id' => 999, 'created_at' => $time_stamp, 'updated_at' => $time_stamp]);
  /**
   * [$msg description]
   * @var string
   */
  $msg ='
<span class="t-icon" style="display: block;">
    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 142 142">
      <path fill-rule="evenodd" clip-rule="evenodd" fill="#0BD38A" d="M49.633 56.134c-5.242-6.092-11.356-8.869-17.969-2.783-6.418 5.905-4.55 12.423.578 18.462 7.943 9.357 15.768 18.82 23.78 28.121 7.299 8.473 15.961 8.13 22.021-1.19 13.361-20.549 52.117-81.182 54.777-85.422 2.114-3.375 1.609-8.397 2.316-12.656-3.816 1.83-7.67 3.596-11.429 5.545-.805.418-1.251 1.521-1.886 2.285-2.225 2.674-41.491 49.854-55.513 66.698-6.258-7.148-11.512-13.064-16.675-19.06zm78.896-25.258c-2.97 3.426-5.52 7.127-7.235 11.311a57.664 57.664 0 0 1 7.748 28.917c0 32.007-26.036 58.043-58.043 58.043-32.002 0-58.043-26.036-58.043-58.043s26.041-58.047 58.043-58.047a57.661 57.661 0 0 1 30.669 8.8c2.629-3.169 5.289-6.314 8.274-9.154C98.789 5.238 85.397.866 70.999.866 32.271.866.766 32.371.766 71.102c0 38.727 31.505 70.234 70.233 70.234 38.727 0 70.235-31.505 70.235-70.232 0-14.955-4.707-28.826-12.705-40.228z"/>
    </svg>
  </span>
  <div class="t-heading-block">
  <h2 class="t-heading-text">Thank you</h2>
  <p class="received-para">your payment has been received</p>
  </div>
  <p class="conf-msg">Please check your email for your order confirmation</p>';
}
catch(Stripe_CardError $e) {
  $error = TRUE;
  $insertData = [
    'order_id'   => $order_id,
    'error'      => $e,
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp,
  ];
  insert('tbl_temp_error', $insertData);
} catch (Stripe_InvalidRequestError $e) {
  $error = TRUE;
  $insertData = [
    'order_id'   => $order_id,
    'error'      => $e,
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp,
  ];
  insert('tbl_temp_error', $insertData);
  // Invalid parameters were supplied to Stripe's API
} catch (Stripe_AuthenticationError $e) {
  $error = TRUE;
  $insertData = [
    'order_id'   => $order_id,
    'error'      => $e,
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp,
  ];
  insert('tbl_temp_error', $insertData);
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
} catch (Stripe_ApiConnectionError $e) {
  $error = TRUE;
  $insertData = [
    'order_id'   => $order_id,
    'error'      => $e,
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp,
  ];
  insert('tbl_temp_error', $insertData);
  // Network communication with Stripe failed
} catch (Stripe_Error $e) {
  $error = TRUE;
  $insertData = [
    'order_id'   => $order_id,
    'error'      => $e,
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp,
  ];
  insert('tbl_temp_error', $insertData);
  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {
  $error = TRUE;
  $insertData = [
    'order_id'   => $order_id,
    'error'      => $e,
    'created_at' => $time_stamp,
    'updated_at' => $time_stamp,
  ];
  insert('tbl_temp_error', $insertData);
  // Something else happened, completely unrelated to Stripe
}
?>
<?php
require_once('includes/vars.php');
?>
<!DOCTYPE html>
<html lang="">
<style>
.thank-you-box, .t-content-block { position: relative; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -ms-flex-align: center; align-items: center;}
.thank-you-box { background-color: transparent; border: 34px solid #eeeeee; max-width: 780px; min-height: 650px; margin: 0 auto;}
.t-content-block {  position: relative; -webkit-box-orient: vertical; -webkit-box-direction: normal;  -ms-flex-direction: column; flex-direction: column;}
.t-content-block .t-icon svg {  display: -webkit-inline-box;  display: -ms-inline-flexbox;  display: inline-flex; width: 142px; height: 142px;}
.t-heading-block { display: inline-block; text-align: center;}
.thank-you-box .t-box-inner { position: relative; /*margin-top: 130px;*/}
.t-heading-block .t-heading-text { font-size: 50px; font-weight: 500; color: #000000; line-height: 50px; text-transform: capitalize; margin: 20px 0px 10px;}
.t-heading-block .received-para { font-size: 22px; color: #000000; line-height: 22px; font-weight: 500;}
.t-content-block .conf-msg { position: relative; font-size: 17px; color: #898989; font-weight: 400; padding: 5px 0px 0px;}
.t-content-block .conf-msg:before { content: '';  position: absolute; left: 0px;  top: 0px; width: 100%;  height: 1px;  background-color: #eeeeee;}
.thank-you-box .t-web-name { position: absolute; bottom: 0px; margin: 0 auto 15px;}
.thank-you-box .t-web-name {  font-size: 13px;  color: #9d00ef; line-height: 13px;  font-weight: 400;}
@media (max-width: 767px) {
  .t-content-block .t-icon svg {width: 120px; height: 120px; }
  .t-heading-block .t-heading-text {font-size: 40px; line-height: 40px;margin: 20px 0px 0px;}
  } /*767px*/
  @media (max-width: 500px) {
    .thank-you-box {min-height: 500px;border: 15px solid #eeeeee;}
    .t-content-block .t-icon svg {  width: 85px; height: 85px;}
    .t-box-inner .t-content-block {padding: 0 10px; }
    .t-heading-block .received-para, .t-content-block .conf-msg {font-size: 16px; }
    } /* 500px */
  </style>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>British Assignment Writers</title>
  <meta name="description" content="">
  <meta name="keywords" content="">

  <?php  include'includes/canonical.php'; ?>
  <?php include 'includes/css.php'; ?>

</head>
<body>

  <?php include 'includes/headersection-innerpages.php'; ?>

  <div class="inner-page contactpage">

  <!--End-header-->
  <section class="content-area-inner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- ===== Thank You Box===== -->
          <div class="thank-you-box">
            <div class="t-box-inner">
              <div class="t-content-block">
                <?php if ($error): ?>
                <span class="t-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
                  <path fill-rule="evenodd" clip-rule="evenodd" fill="#EA5050" d="M104.722 41.787c-1.113-2.688-3.896-4.638-6.617-4.638-1.707 0-3.286.713-4.692 2.12L70.967 61.711 48.538 39.268c-1.393-1.392-2.951-2.098-4.633-2.098-2.711 0-5.494 1.968-6.618 4.678-.601 1.449-1.194 4.297 1.587 7.083l22.445 22.43-22.445 22.444c-2.78 2.781-2.19 5.623-1.59 7.07 1.123 2.711 3.907 4.679 6.62 4.679 1.682 0 3.241-.706 4.634-2.098l22.429-22.43 22.446 22.431c1.389 1.392 2.946 2.097 4.627 2.097 2.714 0 5.498-1.969 6.62-4.682.598-1.446 1.187-4.288-1.598-7.068l-22.43-22.443 22.43-22.43c2.842-2.841 2.26-5.696 1.66-7.144zM71 .875C32.333.875.875 32.333.875 71S32.333 141.125 71 141.125 141.125 109.667 141.125 71 109.667.875 71 .875zm0 128.25c-32.05 0-58.125-26.075-58.125-58.125S38.95 12.875 71 12.875 129.125 38.95 129.125 71 103.05 129.125 71 129.125z"/>
                </svg>
              </span>
                  <div class="t-heading-block">
                    <h2 class="t-heading-text">Sorry</h2>
                    <p class="received-para">Something went wrong!</p>
                  </div>
                  <?php else: ?>
                    <?= $msg;?>
                  <?php endif ?>
                </div>
              </div>
              <span class="t-web-name">
                <?php echo $domain_name; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
    </section>
<!--footer-->
  <?php include 'includes/footerSection.php'; ?>
</body>
</html>


