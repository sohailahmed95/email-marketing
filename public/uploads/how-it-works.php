<?php
require_once('includes/vars.php');
$pageClass = "how-it-works";
?>
<?php
$title = "British Dissertation Writers - How It Works";
$description = "The process of British Dissertation Writers is simple and quick. Get the best dissertation help.";
$linki="https://www.britishdissertationwriters.co.uk/how-it-works.php";
$keywords="Best Dissertation Writing Service, Dissertation Help, Dissertation Writing Service, UK Dissertations, Write My Dissertation, Professional Dissertation Writers, Custom Dissertations";
?>
<?php include 'includes/header.php'; ?>
</head>
<body>
 <?php include 'includes/top-area.php'; ?>
 <?php include 'includes/live-chat-right.php'; ?>
 <!-- ===== banner area===== -->
 <section class="banner-area top-area-inner">
  <div class="container">
   <div class="logo-section">
     <div class="row">
      <div class="col-md-4">
       <a href="./" class="mainlogo">
        <img src="images/main-logo.png" alt="";>
      </a>
    </div>
    <div class="col-md-8">
     <?php include 'includes/navigation.php'; ?> 
   </div>
 </div>
</div>
</div>
</section>
<!-- ===== header area===== -->
<!-- ===== banner inner area===== -->
<section class="banner-inner" style="background-image:url(images/banner-inner01.jpg);">
  <div class="container">
    <div class="banner-inner">How It Works</div>
  </div>
</section>
<!-- breadcrumb -->
<div class="brd-crmb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo $base_url; ?>">Home</a></li>
          <li class="active">How It Works</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<?php //include 'includes/right-side-form.php'; ?>
<!-- ===== quote-section for ipdad and mobile area===== -->
<?php //include 'includes/form-area-ipad-mobile.php'; ?>
<!-- ===== quote-section for ipdad and mobile area===== -->
<!-- ===== main area===== -->
<?php //include 'includes/side-pop-up-form.php'; ?>
<section class="content-inner-main">
 <section class="content-inner">
   <div class="inner-page">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="inner-content">
            <p>British Dissertation Writers has an easy procedure to provide you with excellent dissertation writing help. To finish the process and get perfect dissertation help, you are required to complete three essential steps:</p>
            <ul class="innerpagelist">
              <li>Fill out the order form and supply the essential data. We need the basic details to complete the transaction.</li>
              <li>As we get the order, you will be sent word through email and will be called to pay the sum. This routine step is followed all over the world in the online industry. While you’re making your payments, our authors in the meanwhile will start working on your specifications.</li>
              <li>As we receive the notice regarding the payment for your order, we take no time to deliver the work to you within the deadline. You will experience perfect work which is excellent in quality.</li>
            </ul>
            <p>For further information and detail, call us at <span class="phonenum"></span>,  or email us at: <a href="mailto:info@britishdissertationwriters.co.uk">info@britishdissertationwriters.co.uk.</a></p>
          </div>
        </div>
        <div class="col-md-3">
         <?php include 'includes/form-desktop-inner.php'; ?>
         <div class="side-bnrs"> 
         </div>
       </div>
     </div>
   </div>
 </div>
</section>
</section>
<?php include 'includes/footer.php'; ?>
