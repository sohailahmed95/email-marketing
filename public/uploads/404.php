<?php
    require_once('includes/vars.php');
    $title = "404 error";
    $description = "";
    $linki="https://www.britishdissertationwriters.co.uk/404.php";
    $keywords="";
?>
<?php include 'includes/header.php'; ?>
   <body>
   <?php include 'includes/top-area.php'; ?>
   <!-- ===== banner area===== -->
   <section class="banner-area top-area-inner">
      <div class="container">
         <div class="logo-section">
         <div class="row">
            <div class="col-md-6">
               <a href="./" class="mainlogo">
                  <img src="images/main-logo.png" alt="";>
               </a>
            </div>
            <div class="col-md-6">

                 <?php include 'includes/navigation.php'; ?> 
            </div>
         </div>
         </div>
        
         
      </div>
   </section>
   <!-- ===== header area===== -->
   
   <!-- ===== banner inner area===== -->
   <section class="banner-inner" style="background-image:url(images/banner-inner01.jpg);">
      <div class="container">
          <div class="banner-inner">404 error</div>
      </div>
   </section>
   <!-- breadcrumb -->

<div class="brd-crmb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="./">Home</a></li>
          <li class="active">404 error</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- ===== quote-section for ipdad and mobile area===== -->
   <?php //include 'includes/form-area-ipad-mobile.php'; ?>
   <!-- ===== quote-section for ipdad and mobile area===== -->
   <!-- ===== main area===== -->
   <section class="content-inner-main">
     <section class="content-inner">
         <div class="inner-page">
        <div class="container">
              <h2 class="heading2 title-sp">page not found</h2>
          <div class="content-sp">
          <p>Sorry for Inconvenience.
          The page that you are looking for is not found, please browse different links or find your desire page in site map
          For further assistance, you can contact us at 
          <span class="phoneNumber">2030-34-1416</span>
          or email us at 
          <a href="mailto:info@britishdissertationwriters.co.uk" class="mailto"><span class="email">info@britishdissertationwriters.co.uk</span></a>
          </p>
          </div>
          
        </div>
</div>

<!-- End inner content -->
     </section>
  </section>

  <?php include 'includes/footer.php'; ?>
